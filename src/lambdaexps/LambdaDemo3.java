package lambdaexps;

interface Calculator{
    void calculate(double a,double b);
}
public class LambdaDemo3 {
    public static void main(String[] args) {
        //lambda expression with multiple parameters
        Calculator c1=(no1,no2)-> System.out.println(no1+no2);
        c1.calculate(45,75);
    }
}
