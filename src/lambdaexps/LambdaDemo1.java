package lambdaexps;

interface Master{
    void test();
}
public class LambdaDemo1 {
    public static void main(String[] args) {
        //before Java8
        Master m1=new Master() {
            @Override
            public void test() {
                System.out.println("TEST METHOD");
            }
        };
        //from Java8
        Master m2=()->{
            System.out.println("TEST METHOD");
        };
        m2.test();
    }
}
