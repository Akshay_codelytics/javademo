package lambdaexps;
interface Mobile{
    String getCompany(String companyName);
}
public class LambdaDemo2 {
    public static void main(String[] args) {
        //Calling parameterized method
        //Option-1
        Mobile m1=(name)->{
            return "Company Name: "+name;
        };
        System.out.println(m1.getCompany("SONY"));

        //can omit brackets if only one statement is present
        //Option-2
        Mobile m2=name->"Company Name: "+name;
        System.out.println(m2.getCompany("LG"));
    }
}
