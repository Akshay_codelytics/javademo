package collectionprograms;

public class AppointmentTracker {
    String doctorName;
    String hospitalName;
    int appointmentDate;

    public AppointmentTracker(String doctorName, String hospitalName, int appointmentDate) {
        this.doctorName = doctorName;
        this.hospitalName = hospitalName;
        this.appointmentDate = appointmentDate;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof AppointmentTracker){
            return ((AppointmentTracker) o).appointmentDate==appointmentDate;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.appointmentDate;
    }
}
