package collectionprograms;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class StudentResultSimulator {
    static Map<String,Integer> score=new HashMap<String,Integer>();
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        boolean flag=true;
        while(flag){
            System.out.println("ENTER SUBJECT NAME");
            String subject=sc1.next();
            System.out.println("ENTER MARKS");
            int marks= sc1.nextInt();
            addStudentMarks(subject,marks);
            System.out.println("WANT TO ADD NEW ENTRY");
            System.out.println("1:YES \t  2:NO");
            int choice= sc1.nextInt();
            if(choice==2){
                displayResult();
                flag=false;
            }
        }
    }
    static void addStudentMarks(String subject,int marks){
        score.put(subject,marks);
    }
    static void displayResult(){
        Set<Map.Entry<String,Integer>> entries=score.entrySet();
        int sum=0;
        System.out.println("SUBJECT\t\tMARKS");
        System.out.println("================================");
        for(Map.Entry<String,Integer> entry:entries){
            System.out.println(entry.getKey()+"\t\t"+entry.getValue());
            sum=sum+entry.getValue();
        }
        double percentage=sum/entries.size();
        System.out.println("================================");
        System.out.println("FINAL PERCENTAGE IS "+percentage);
        System.out.println("================================");
        if(percentage>35.0){
            System.out.println("RESULT IS : PASS");
        }
        else{
            System.out.println("RESULT IS : FAIL");
        }
    }
}
