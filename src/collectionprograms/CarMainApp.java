package collectionprograms;

import java.util.Scanner;

public class CarMainApp {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        CarDealership dealership=new CarDealership();
        boolean status=true;
        while (true){
            System.out.println("1: Add New Car");
            System.out.println("2: View Car Details");
            System.out.println("3: Exit");
            int choice= sc1.nextInt();
            sc1.nextLine();
            switch (choice){
                case 1:
                    String company=promptForCompany(sc1);
                    String model=promptForModel(sc1);
                    BodyType bodyType=promptForBodyType(sc1);
                    int productionYear=promptForProductionYear(sc1);
                    double price=promptForPrice(sc1);
                    dealership.addNewCar(new Car(company,model,bodyType,productionYear,price));
                    break;
                case 2:
                    dealership.viewAllCars();
                    break;
                default:
                    status=false;
            }
        }


    }
    //validate price input
    private static double promptForPrice(Scanner sc1) {
        while (true){
            System.out.println("Enter Price");
            double price= sc1.nextDouble();
            if (!(price<Car.MIN_PRICE || price>Car.MAX_PRICE)){
                return price;
            }
        }
    }
    //validate production year input
    private static int promptForProductionYear(Scanner sc1) {
        while (true){
            System.out.println("Enter Production Year");
            int year= sc1.nextInt();
            if (year>Car.MIN_YEAR){
                return year;
            }
        }
    }
    //validate body type input
    private static BodyType promptForBodyType(Scanner sc1) {
        while (true){
            System.out.println("Enter Body Type");
            String type= sc1.nextLine();
            try {
                return BodyType.valueOf(type.toUpperCase());
            }catch (IllegalArgumentException i){
                System.out.println();
            }
        }
    }
    //validate model input
    private static String promptForModel(Scanner sc1) {
        while (true){
            System.out.println("Enter Model Name");
            String model=sc1.nextLine();
            if (!(model==null||model.isBlank())){
                return model;
            }
        }
    }
    //validate company input
    private static String promptForCompany(Scanner sc1) {
        while (true){
            System.out.println("Enter Company Name");
            String company=sc1.nextLine();
            if (!(company==null||company.isBlank())){
                return company;
            }
        }
    }

}
