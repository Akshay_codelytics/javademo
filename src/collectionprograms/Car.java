package collectionprograms;

public class Car {
    private String company;
    private String model;
    private BodyType bodyType;
    private int productionYear;
    private double price;

    public static final int MIN_YEAR=1950;
    public static final double MIN_PRICE=500000;
    public static final double MAX_PRICE=3000000;

    public Car(String company, String model, BodyType bodyType, int productionYear, double price) {
        setCompany(company);
        setModel(model);
        setBodyType(bodyType);
        setProductionYear(productionYear);
        setPrice(price);
    }
    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public BodyType getBodyType() {
        return bodyType;
    }

    public void setBodyType(BodyType bodyType) {
        this.bodyType = bodyType;
    }

    public int getProductionYear() {
        return productionYear;
    }

    public void setProductionYear(int productionYear) {
        this.productionYear = productionYear;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return company+"\t"+model+"\t"+bodyType+"\t"+productionYear+"\t"+price;
    }
}
