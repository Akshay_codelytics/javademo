package collectionprograms;

import java.util.HashSet;

public class MainApp1 {
    public static void main(String[] args) {
        HospitalManagement hs1=new HospitalManagement();
        hs1.addDoctors("SNEHA");//0
        hs1.addDoctors("PRATIK");//1
        hs1.addDoctors("AKSHAY");//2

        hs1.addHospitals("JOSHI");//0
        hs1.addHospitals("SHERDIWALA");//1
        hs1.addHospitals("KULKARNI");//2

        hs1.addAppointmentDate(5);//0
        hs1.addAppointmentDate(12);//1
        hs1.addAppointmentDate(14);//2

        HashSet<AppointmentTracker> tracker=new HashSet<>();
        AppointmentTracker t1=new AppointmentTracker(hs1.doctors.get(1),hs1.hospitals.get(2),hs1.appointments.get(2));
        AppointmentTracker t2=new AppointmentTracker(hs1.doctors.get(1),hs1.hospitals.get(2),hs1.appointments.get(2));
        tracker.add(t1);
        tracker.add(t2);

        for (AppointmentTracker t:tracker){
            System.out.println(t.doctorName+"\t"+t.hospitalName+"\t"+t.appointmentDate);
        }
    }
}
