package collectionprograms;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class TokenSimulator {
    static Queue<Integer> tokens=new LinkedList<Integer>();
    static int token=101;
    public static void main(String[] args) throws InterruptedException {
        Scanner sc1=new Scanner(System.in);
        boolean addCustomer=true;
        System.out.println("=================================");
        while(addCustomer){
            System.out.println("1:GENERATE NEW TOKEN");
            System.out.println("2:PROCESS TOKENS");
            System.out.println("3:EXIT");
            int choice= sc1.nextInt();
            switch (choice){
                case 1:tokenGenerator();
                        break;
                case 2:processTokens();
                        break;
                case 3:addCustomer=false;
                        break;
            }
            System.out.println("=================================");
        }
    }
    static void tokenGenerator() {
        tokens.offer(token);
        System.out.println("TOKEN NO IS "+token);
        token++;
    }

    static void processTokens() throws InterruptedException {
        System.out.println("PROCESSING ORDER FOR TOKEN NO "+tokens.peek());
        System.out.println("......");
        Thread.sleep(5000);
        System.out.println("ORDER PROCESSED FOR TOKEN NO "+tokens.poll());
    }
}
