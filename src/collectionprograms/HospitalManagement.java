package collectionprograms;

import java.util.ArrayList;
import java.util.List;

public class HospitalManagement {
    List<String> doctors=new ArrayList<>();
    List<String> hospitals=new ArrayList<>();
    List<Integer> appointments=new ArrayList<>();
    void addDoctors(String doctorName){
        doctors.add(doctorName);
    }

    void addHospitals(String hospitalName){
        hospitals.add(hospitalName);
    }

    void addAppointmentDate(Integer appointmentDate){
        appointments.add(appointmentDate);
    }
}
