package collectionprograms;

import java.util.ArrayList;

public class StockDAO {
    ArrayList<Stock> stocks=new ArrayList<>();

    {
        stocks.add(new Stock("Reliance",2300,5));
        stocks.add(new Stock("Coal India",150,15));
        stocks.add(new Stock("HDFC",1600,4));
        stocks.add(new Stock("Britania",4800,2));
        stocks.add(new Stock("IRCTC",850,13));
    }

    void searchStock(String name){
        for (Stock s:stocks){
            if (s.name.equals(name)){
                double total=s.price*s.qty;
                System.out.println("STOCK NAME IS "+name);
                System.out.println("TOTAL INVESTED AMOUNT "+total);
            }
        }
    }
    void sellStock(String name,int qty){
        for (Stock s:stocks){
            if (s.name.equals(name) && s.qty<=qty){
                double total=s.price*qty;
                s.qty-=qty;
                System.out.println("TOTAL AMOUNT AFTER SELLING STOCKS "+total);
            }
        }
    }
    void investment(){
        double totalInvestment=0.0;
        for (Stock s:stocks){
            totalInvestment+=s.price;
        }
        System.out.println("TOTAL INVESTED AMOUNT IN ALL STOCKS "+totalInvestment);
    }
    void updateStockDetails(String name,double price,int qty){

    }
    void portfolio(){

    }
}
