package collectionprograms;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class GroceryShop {
    HashMap<String,Double> groceryItems;
    HashMap<String,Double> addedItems= new HashMap<>();
    static double finalAmount;

    //initialize all products from store
    {
        groceryItems = new HashMap<>();
        groceryItems.put("TOMATO",25.00);
        groceryItems.put("ONIONS",35.00);
        groceryItems.put("POTATO",40.00);
    }

    void payBill(double amt) {
        if(amt==finalAmount){
            finalAmount-=amt;
        }else{
            System.out.println("ENTER VALID AMOUNT");
        }
    }

    void checkout() {
        Set<Map.Entry<String,Double>> items=addedItems.entrySet();
        System.out.println("ITEM\tU.PRICE\tTOTAL");
        System.out.println("========================================");
        for(Map.Entry<String, Double> e:items){
            System.out.println(e.getKey()+"\t"+groceryItems.get(e.getKey())+"\t"+e.getValue());
            finalAmount=finalAmount+e.getValue();
        }
        System.out.println("========================================");
        System.out.println("FINAL BILL AMOUNT IS "+finalAmount);
    }

    void displayCart() {
        Set<Map.Entry<String,Double>> items=addedItems.entrySet();
        System.out.println("ITEM\tU.PRICE\tTOTAL");
        System.out.println("========================================");
        for(Map.Entry<String, Double> e:items){
            System.out.println(e.getKey()+"\t"+groceryItems.get(e.getKey())+"\t"+e.getValue());
        }
    }

    void addToCart(String name,int qty) {
        boolean b=groceryItems.containsKey(name);
        if(b){
            addedItems.put(name,groceryItems.get(name)*qty);
            System.out.println("ITEM ADDED SUCCESSFULLY");
        }
        else{
            System.out.println("ITEM NOT PRESENT");
        }
    }

    void removeFromCart(String name){
        if(addedItems.containsKey(name)){
           addedItems.remove(name);
            System.out.println("ITEM REMOVED SUCCESSFULLY");
        }else {
            System.out.println("ITEM NOT PRESENT");
        }
    }

    void listOfGroceryItems() {


        Set<Map.Entry<String,Double>> items=groceryItems.entrySet();
        for(Map.Entry<String,Double> e:items){
            System.out.println(e.getKey()+"\t"+e.getValue());
        }
    }
}
