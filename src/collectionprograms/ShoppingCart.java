package collectionprograms;

import java.util.Scanner;

public class ShoppingCart {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        GroceryShop gShop=new GroceryShop();
        boolean flag=true;

        while(flag){
            System.out.println("1:VIEW GROCERY ITEMS");
            System.out.println("2:ADD ITEM TO CART");
            System.out.println("3:REMOVE ITEM FROM CART");
            System.out.println("4:VIEW CART");
            System.out.println("5:CHECKOUT");
            System.out.println("6:PAY BILL");
            System.out.println("7:EXIT");
            int choice= sc1.nextInt();
            System.out.println("========================================");
            switch (choice){
                case 1: gShop.listOfGroceryItems();
                        break;
                case 2:
                        System.out.println("ENTER ITEM NAME TO ADD IN THE CART");
                        String name= sc1.next();
                        System.out.println("ENTER QTY");
                        int qty=sc1.nextInt();
                        gShop.addToCart(name,qty);
                        break;
                case 3:
                        System.out.println("ENTER PRODUCT NAME TO BE REMOVED");
                        String pName= sc1.next();
                        gShop.removeFromCart(pName);
                        break;
                case 4: gShop.displayCart();
                        break;
                case 5: gShop.checkout();
                        break;
                case 6:
                        System.out.println("ENTER AMOUNT TO BE PAID");
                        double amt= sc1.nextDouble();
                        gShop.payBill(amt);

                case 7: if(gShop.addedItems.isEmpty()|| GroceryShop.finalAmount ==0){
                            System.out.println("THANKS FOR SHOPPING WITH US");
                            flag=false;
                            gShop.addedItems.clear();
                            break;
                        }else{
                        System.out.println("FIRST PAY YOUR BILL");
                        }
            }
            System.out.println("========================================");
        }
    }


}
