package collectionprograms;

import java.util.Scanner;

public class StockSimulator {
    public static void main(String[] args) {
        StockDAO s1=new StockDAO();
        Scanner sc1=new Scanner(System.in);
        boolean flag=true;
        while (flag){
            System.out.println("Select Mode Of Operation");
            System.out.println("1: Search Stock");
            System.out.println("2: Sell Stock");
            System.out.println("3: Total Investment");
            System.out.println("4: Update Stock Details");
            System.out.println("5: Portfolio");
            System.out.println("6: Exit");
            int choice= sc1.nextInt();
            String name;
            int qty;
            double price;
            switch (choice){
                case 1:
                    System.out.println("Enter Name of the stock");
                    sc1.nextLine();
                    name= sc1.nextLine();
                    s1.searchStock(name);
                    break;
                case 2:
                    System.out.println("Enter Name of the stock");
                    name= sc1.nextLine();
                    System.out.println("Enter qty to be sell out");
                    qty= sc1.nextInt();
                    s1.sellStock(name,qty);
                    break;
                case 3: s1.investment();
                        break;
                case 4: s1.updateStockDetails("Britania",2400,3);
                        break;
                case 5: s1.portfolio();
                        break;
                case 6: flag=false;
                        break;
            }
        }
    }
}
