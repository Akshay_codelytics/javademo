
package collectionprograms;

import java.util.Scanner;

public class ProductListManager {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        ProductDAO dao=new ProductDAO();
        boolean repeat=true;
        while (repeat){
            System.out.println("============================");
            System.out.println("Select Mode of Operation");
            System.out.println("1:ADD PRODUCT\n2:DISPLAY PRODUCTS\n3:DELETE PRODUCT\n4:EXIT");
            int choice= sc1.nextInt();
            switch (choice){
                case 1:
                    System.out.println("Enter Product Id");
                    int id= sc1.nextInt();
                    System.out.println("Enter Product Name");
                    String name= sc1.next();
                    System.out.println("Enter Product Price");
                    double price= sc1.nextDouble();
                    dao.addProducts(id,name,price);
                    break;
                case 2:
                    dao.displayProducts();
                    break;
                case 3:
                    System.out.println("Enter Product Id");
                    int pid= sc1.nextInt();
                    dao.deleteProducts(pid);
                    break;
                case 4:
                    repeat=false;
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + choice);
            }
        }
    }

}
