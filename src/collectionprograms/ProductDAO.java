package collectionprograms;
import java.util.ArrayList;
import java.util.Iterator;
public class ProductDAO {
    ArrayList<Product> productList=new ArrayList<>();
    void addProducts(int id, String name, double price){
        Product p=new Product(id,name,price);
        productList.add(p);
        System.out.println("Product Added Successfully");
    }
    void displayProducts(){
        System.out.println("ID\tNAME\tPRICE");
        System.out.println("============================");
        for (Product p:productList){
            System.out.println(p.productId+"\t"+p.productName+"\t"+p.productPrice);
        }
    }
    void deleteProducts(int pid){
        Iterator<Product> itr=productList.iterator();
        while (itr.hasNext()){
           if (itr.next().productId==pid){
               itr.remove();
               System.out.println("Product Deleted Successfully");
           }
        }
    }
}
