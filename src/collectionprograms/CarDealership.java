package collectionprograms;

import java.util.ArrayList;

public class CarDealership {
    private final ArrayList<Car> cars=new ArrayList<>();

    public void addNewCar(Car c){
        cars.add(c);
        System.out.println("Car Added Successfully");
    }

    public void viewAllCars() {
        System.out.println("COMPANY\tMODEL\tTYPE\tYEAR\tPRICE");
        System.out.println("=============================================");
        for (Car c:cars){
            System.out.println(c);
            System.out.println("---------------------------------------------");
        }
    }
}
