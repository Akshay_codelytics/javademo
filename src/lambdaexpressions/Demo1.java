package lambdaexpressions;

public class Demo1 {
    public static void main(String[] args) {
        MyExpression displayMessage = () -> System.out.println("Hello...");
        displayMessage.test();
    }
}
interface MyExpression{
    void test();
}
