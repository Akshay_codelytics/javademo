package patterns;
//P.NO=140

public class Pattern7 {
    public static void main(String[] args) {
        int lines=9;
        int count=5;
        int space=4;
        int width=0;
        int mid=(lines+1)/2;

        for (int r=1;r<=lines;r++){
            for (int s=1;s<=space;s++){
                System.out.print(" ");
            }
            for (int c=1;c<=count;c++){
                System.out.print("*");
            }
            for (int s=1;s<=width;s++){
                System.out.print(" ");
            }
            for (int c=1;c<=count;c++){
                System.out.print("*");
            }
            System.out.println();
            if(r<mid){
                space--;
                width+=2;
            }else{
                space++;
                width-=2;
            }
        }
    }
}
