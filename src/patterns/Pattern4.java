package patterns;
//P.NO=22
public class Pattern4 {
    public static void main(String[] args) {
        int lines=5;
        int count=5;
        int space=0;

        for (int r=1;r<=lines;r++){
            for (int s=1;s<=space;s++){
                System.out.print(" ");
            }
            for (int c=1;c<=count;c++){
                System.out.print("*");
            }
            System.out.println();
            space++;
        }
    }
}
