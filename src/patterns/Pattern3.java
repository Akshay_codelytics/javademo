package patterns;
//P.NO=21
public class Pattern3 {
    public static void main(String[] args) {
        int lines=5;
        int count=5;
        int space=4;

        for (int r=1;r<=lines;r++){
            for (int s=1;s<=space;s++){
                System.out.print(" ");
            }
            for (int c=1;c<=count;c++){
                System.out.print("*");
            }
            System.out.println();
            space--;
        }
    }
}
