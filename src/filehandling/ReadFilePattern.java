package filehandling;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;

public class ReadFilePattern {
    public static void main(String[] args) throws FileNotFoundException {
        // arraylist to store strings
        List<String> listOfStrings
                = new ArrayList<String>();

        // load content of file based on specific delimiter
        Scanner sc = new Scanner(new FileReader("E://Java files//demo.txt"));
        String str;

        // checking end of file
        while (sc.hasNext()) {
            str = sc.next();
            // adding each string to arraylist
            listOfStrings.add(str);
        }
        //convert arraylist to set
        HashSet<String> data=new HashSet<>(listOfStrings);
        // convert hashset to array
        String[] array = data.toArray(new String[0]);
        // print each string in array
        /*for (String eachString : array) {
            System.out.print(eachString+" ");
        }*/
        System.out.println("==========================================");
        for (int a=0;a< array.length;a++){
            if (array[a].length()>2 && !array[a].matches("[0-9]")){
                System.out.println(a+1+" "+array[a].substring(0,1).toUpperCase()+array[a].replace(Character.toString(array[a].charAt(0)),""));
            }
        }
    }
}
