package datastructures;

public class PlayerNode{
    String data;
    PlayerNode next;

    public PlayerNode(String data) {
        this.data = data;
    }
}
