package datastructures;

import java.util.Scanner;

public class MainApp2 {
    public static void main(String[] args) {
        MusicPlayer player=new MusicPlayer();
        Scanner sc1=new Scanner(System.in);
        boolean status=true;
        while (status){
            System.out.println("1:Start Playlist");
            System.out.println("2:Play Next Song");
            System.out.println("3:Play Previous Song");
            System.out.println("4:Exit");
            int choice= sc1.nextInt();
            switch (choice) {
                case 1 -> player.startPlayList();
                case 2 -> player.playNextSong();
                case 3 -> player.playPreviousSong();
                default -> status = false;
            }
        }
    }
}
