package datastructures;
//Song node
public class Song {
    String songName;
    String artist;

    public Song(String songName, String artist) {
        this.songName = songName;
        this.artist = artist;
    }

    @Override
    public String toString() {
        return "SONG:"+songName+"\tARTIST:"+artist;
    }
}
