package datastructures;

import java.util.Scanner;
import java.util.Stack;

public class StackDemo1 {
    static String current_option;
    static Stack<String> forward_stack=new Stack<>();
    static Stack<String> backward_stack=new Stack<>();

    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        boolean status=true;
        while (status){
            System.out.println("1: Open Page");
            System.out.println("2: Go Backward");
            System.out.println("3: Go forward");
            System.out.println("4: Close browser");
            int choice= sc1.nextInt();
            if (choice==1){
                current_option= sc1.next();
                openPage(current_option);
            }
            else if (choice==2){
                goBackward();
            }else if (choice==3){
                goForward();
            }else if (choice==4){
                status=false;
            }
            System.out.println("===========================================");
        }
    }
    static void goForward(){
        if (!forward_stack.isEmpty()){
            System.out.println(forward_stack.peek()+" Opened Successfully");
            backward_stack.push(forward_stack.pop());
        }else {
            System.out.println("PAGE NOT AVAILABLE");
        }
    }
    static void goBackward(){
        if (backward_stack.size() > 1){
            forward_stack.push(backward_stack.pop());
            System.out.println(backward_stack.peek()+" Opened Successfully");
        }else {
            System.out.println("PAGE NOT AVAILABLE");
        }
    }
    static void openPage(String option){
        if (!option.equals("")){
            backward_stack.push(option);
            System.out.println(backward_stack.peek()+" Opened Successfully");
        }
    }

}
