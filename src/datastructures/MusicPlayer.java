package datastructures;

import java.util.LinkedList;
import java.util.ListIterator;

public class MusicPlayer {
    LinkedList<Song> playList;
    ListIterator<Song> currentSongIterator;

    {
        playList=new LinkedList<>();
        playList.add(new Song("MORYA MORYA","AJAY"));
        playList.add(new Song("KHEL MANDALA","AJAY"));
        playList.add(new Song("HUKUM","ANIRUDDHA"));
        playList.add(new Song("YAD LAGALA","AJAY"));
    }

    public void startPlayList(){
        if (playList.isEmpty()){
            System.out.println("The Playlist is empty. Add songs to Playlist");
            return;
        }
        currentSongIterator= playList.listIterator();
        System.out.println("PLAYING "+currentSongIterator.next());
    }

    public void playNextSong(){
        if (currentSongIterator!=null && currentSongIterator.hasNext()){
            System.out.println("PLAYING "+currentSongIterator.next());
        }else {
            System.out.println("Playlist is empty");
        }
    }

    public void playPreviousSong(){
        if (currentSongIterator!=null && currentSongIterator.hasPrevious()){
            System.out.println("PLAYING "+currentSongIterator.previous());
        }else {
            System.out.println("Playlist is empty");
        }
    }

}
