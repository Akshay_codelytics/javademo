package datastructures;

public class PlayerList {
   private PlayerNode currentPlayer;
   //add new player into list
    public void addNewPlayer(String name){
        PlayerNode newPlayer=new PlayerNode(name);
        //check whether players are available or not
        if (currentPlayer==null){
            //if not then consider first player as a current player
            currentPlayer=newPlayer;
            //assign next player to make it circular
            currentPlayer.next=currentPlayer;
        }else {
            newPlayer.next=currentPlayer.next;
            currentPlayer.next=newPlayer;
            currentPlayer=newPlayer;
        }
    }

    public String getCurrentPlayer(){
        return currentPlayer!=null?currentPlayer.data:null;
    }

    public void nextTurn(){
        if (currentPlayer!=null){
            currentPlayer=currentPlayer.next;
        }
    }
}
