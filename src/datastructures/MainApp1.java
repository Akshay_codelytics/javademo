package datastructures;

public class MainApp1 {
    public static void main(String[] args) {
        PlayerList list=new PlayerList();
        list.addNewPlayer("Akshay");
        list.addNewPlayer("Ajinkya");
        list.addNewPlayer("Omkar");
        list.addNewPlayer("Rakesh");

        System.out.println("CURRENT PLAYER: "+list.getCurrentPlayer());
        list.nextTurn();
        System.out.println("CURRENT PLAYER: "+list.getCurrentPlayer());
        list.nextTurn();
        System.out.println("CURRENT PLAYER: "+list.getCurrentPlayer());
        list.nextTurn();
        System.out.println("CURRENT PLAYER: "+list.getCurrentPlayer());
        list.nextTurn();
        System.out.println("CURRENT PLAYER: "+list.getCurrentPlayer());
    }
}
