package studentarray;

import java.util.Scanner;

public class StudentRepository {
    private Student[] students = new Student[5];

    {
        addStudents();
    }

    private void addStudents(){
        Scanner sc = new Scanner(System.in);
        for(int i = 0; i < students.length; i++) {
            System.out.println("Enter Name");
            String name = sc.next();
            System.out.println("Enter Percentage");
            double percentage = sc.nextDouble();
            students[i] = new Student(i+1, name, percentage);
        }
    }
}
