package studentarray;

public class Student {
    int studentId;
    String studentName;
    double studentPercentage;

    public Student(int studentId, String studentName, double studentPercentage) {
        this.studentId = studentId;
        this.studentName = studentName;
        this.studentPercentage = studentPercentage;
    }
}
