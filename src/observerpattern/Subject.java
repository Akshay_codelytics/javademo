package observerpattern;

public interface Subject {
    void subscribe(Observer o);
    void unsubscribe(Observer o);
    void notifyUpdate(Message m);
}
