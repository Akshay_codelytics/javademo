package observerpattern;

public class MessageObserver implements Observer{

    String observerName;

    public MessageObserver(String observerName) {
        this.observerName = observerName;
    }

    @Override
    public void update(Message m) {
        System.out.println("Message to "+observerName+" :"+m.getMessage());
    }

    @Override
    public void displayName() {
        System.out.println("SUBSCRIBER NAME IS "+observerName);
    }
}
