package observerpattern;

import java.util.Scanner;

public class AccountManager {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        boolean addSubscriber=true;
        MessagePublisher mp=new MessagePublisher();
        MessageObserver mb=null;
        while(addSubscriber){
            System.out.println("1:SUBSCRIBE CHANNEL");
            System.out.println("2:UNSUBSCRIBE CHANNEL");
            System.out.println("3:GET LIST OF SUBSCRIBERS");
            System.out.println("4:SEND NOTIFICATION");
            System.out.println("5:EXIT");
            int choice= sc1.nextInt();
            System.out.println("======================================");
            switch (choice){
                case 1:
                    System.out.println("ENTER YOUR NAME");
                    String name1= sc1.next();
                    mb=new MessageObserver(name1);
                    mp.subscribe(mb);
                    break;

                case 2:
                    System.out.println("ENTER YOUR NAME");
                    String name2= sc1.next();
                    if(mb.observerName.equalsIgnoreCase(name2)){
                        mp.unsubscribe(mb);
                    }else{
                        System.out.println("FIRST SUBSCRIBE TO CHANNEL");
                    }
                    break;

                case 3:
                    mp.showSubscribers();
                    break;

                case 4:
                    System.out.println("ENTER NOTIFICATION MESSAGE");
                    String msg=sc1.next();
                    sc1.nextLine();
                    mp.notifyUpdate(new Message(msg));
                    break;

                case 5:
                    addSubscriber=false;
                    break;
            }
            System.out.println("======================================");
        }
    }
}
