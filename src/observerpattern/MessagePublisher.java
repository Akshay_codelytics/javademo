package observerpattern;

import java.util.ArrayList;
import java.util.List;

public class MessagePublisher implements Subject{
    private List<Observer> allObservers=new ArrayList<Observer>();

    @Override
    public void subscribe(Observer o) {
        allObservers.add(o);
        System.out.println("YOU HAVE SUBSCRIBED TO CHANNEL");
    }

    @Override
    public void unsubscribe(Observer o) {
        allObservers.remove(o);
        System.out.println("YOU HAVE UNSUBSCRIBED CHANNEL");
    }

    @Override
    public void notifyUpdate(Message m) {
        for(Observer o:allObservers){
            o.update(m);
        }
    }

    public void showSubscribers(){
        for(Observer o:allObservers){
            o.displayName();
        }
    }
}
