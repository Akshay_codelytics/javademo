package observerpattern;

public interface Observer {
    void update(Message m);
    void displayName();
}
