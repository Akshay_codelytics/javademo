package observerpattern;

public class ChannelSimulator {
    public static void main(String[] args) {
        MessageObserver m1=new MessageObserver("Akshay");
        MessageObserver m2=new MessageObserver("Ajinkya");
        MessageObserver m3=new MessageObserver("Pratik");

        MessagePublisher mp=new MessagePublisher();
        mp.subscribe(m1);
        mp.subscribe(m2);
        mp.subscribe(m3);

        mp.notifyUpdate(new Message("New video Added"));

        mp.unsubscribe(m3);

        mp.notifyUpdate(new Message("Happy Holi"));
    }
}
