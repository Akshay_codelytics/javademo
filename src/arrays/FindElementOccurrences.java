package arrays;

import java.util.Scanner;

public class FindElementOccurrences {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        int[] arr1={10,20,30,10,45,10};

        System.out.println("Enter Element To Count Occurrences");
        int no= sc1.nextInt();
        boolean status=false;
        int count=0;
        for (int a=0;a< arr1.length;a++){
            if (arr1[a]==no){
                count++;
                status=true;
            }
        }
        if (status){
            System.out.println("Total No Of Occurrences are "+count);
        }else {
            System.out.println("Element not found");
        }
    }
}
