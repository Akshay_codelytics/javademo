package arrays;

import java.util.Scanner;

public class StoreManager {
    static String[] products={"TV","MOBILE","HEADPHONES","AC"};
    static double[] prices={45000.25,25000.25,1500.25,85000.25};
    static int[] stock={50,200,1500,20};

    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Select Product");
        System.out.println("1: TV\n2: MOBILE\n3: HEADPHONES\n4: AC");
        int choice= sc1.nextInt();
        System.out.println("Enter Qty");
        int qty= sc1.nextInt();
        billCalculate(qty,choice);
    }
    static void billCalculate(int qty,int choice){
        double totalAmount=0.0;
        boolean found=false;
        for (int a=0;a< products.length;a++){
            if (a+1==choice && qty<=stock[a]){
                totalAmount=qty*prices[a];
                stock[a]-=qty;
                found=true;
            }
        }
        if (found){
            System.out.println("Bill Amount Is "+totalAmount+" Rs");
        }else {
            System.out.println("Product Not Found or out of stock");
        }
    }

}
