package arrays;

import java.util.Scanner;

public class BillTracker {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter count of products");
        int size= sc1.nextInt();
        double[] productPrices=new double[size];
        System.out.println("Enter Cost of "+size+" Products");
        for (int a=0;a<size;a++){
            productPrices[a]= sc1.nextDouble();
        }
        System.out.println("Enter count of products to be considered for billing");
        int count= sc1.nextInt();

        calculateBill(count,productPrices);
    }
    static void calculateBill(int count, double[] productPrices){
        double total=0.0;
        double discountedTotal=0.0;
        double gstAmount=0.0;
        //sort given array into descending order
        double minValue;
        for (int a=0;a< productPrices.length;a++){
            for (int b=a+1;b< productPrices.length;b++){
                if (productPrices[a]<productPrices[b]){
                    minValue=productPrices[a];
                    productPrices[a]=productPrices[a+1];
                    productPrices[b]=minValue;
                }
            }
        }
        //calculate total bill amount
        for (int a=0;a< productPrices.length;a++){
            total+=productPrices[a];
        }
        //calculate new total based on offer details
        for (int a=0;a< count;a++){
            discountedTotal+=productPrices[a];
        }
        //add 12% GST
        gstAmount=discountedTotal+discountedTotal*0.12;

        System.out.println("TOTAL BILL AMOUNT "+total);
        System.out.println("DISCOUNTED BILL AMOUNT "+discountedTotal);
        System.out.println("FINAL BILL AMOUNT "+gstAmount);
    }
}
