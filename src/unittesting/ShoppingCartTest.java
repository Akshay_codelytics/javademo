package unittesting;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ShoppingCartTest {
    ShoppingCart cart=new ShoppingCart();

    @Test
    public void getSubTotal() {
        assertEquals(929.22,cart.getSubTotal());
    }

    @Test
    public void getTaxAmount() {
        assertEquals(120.79860000000001,cart.getTaxAmount(929.22));
    }

    @Test
    public void getTotalAmount() {
        assertEquals(1050.0186,cart.getTotalAmount(929.22,120.79860000000001));
    }
}