package unittesting;

public class ShoppingCart {
    double[] prices={420.14,358.34,150.74};
    public double getSubTotal(){
        double subTotal=0.0;
        for (double price : prices) {
            subTotal += price;
        }
        return subTotal;
    }

    public double getTaxAmount(double subTotal){
        double taxAmt=0.0;
        taxAmt=subTotal*0.13;
        return taxAmt;
    }

    public double getTotalAmount(double subTotal,double taxAmt){
        return subTotal+taxAmt;
    }
}
