package classtypearray;

public class Employee {
    int employeeId;
    String employeeName;
    double employeeSalary;

    public Employee(int employeeId, String employeeName, double employeeSalary) {
        this.employeeId = employeeId;
        this.employeeName = employeeName;
        this.employeeSalary = employeeSalary;
    }
    void displayData(){
        System.out.println("EMPLOYEE ID IS "+employeeId+"\n"+
                "EMPLOYEE NAME IS "+employeeName+"\n"+
                "EMPLOYEE SALARY IS "+employeeSalary);
    }
}
