package classtypearray;

public class MainApp3 {
    public static void main(String[] args) {
        Pen[] penArray1=new Pen[4];
        penArray1[0]=new SketchPen();
        penArray1[1]=new MarkerPen();
        penArray1[2]=new SketchPen();
        penArray1[3]=new MarkerPen();
        for(int i=0;i<penArray1.length;i++){
            penArray1[i].getPenType();
        }
        int sCount=0;
        int mCount=0;
        for(int i=0;i< penArray1.length;i++){
            if(penArray1[i] instanceof SketchPen){
                sCount++;
            }
            else if(penArray1[i] instanceof MarkerPen){
                mCount++;
            }
        }
        System.out.println("TOTAL NO OF SKETCH PENS ARE "+sCount);
        System.out.println("TOTAL NO MARKER PENS ARE "+mCount);
    }
}
