package classtypearray;

public class MainApp1 {
    public static void main(String[] args) {
        Employee[] employeeArray=new Employee[2];
        Employee e1=new Employee(101,"AKSHAY",40000.25);
        Employee e2=new Employee(102,"AJINKYA",25000.25);

        employeeArray[0]=e1;
        employeeArray[1]=e2;

        for (int i=0;i<employeeArray.length;i++){
            employeeArray[i].displayData();
        }
    }
}
