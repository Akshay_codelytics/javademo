package classtypearray;

import java.util.Scanner;

public class MainApp4 {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        System.out.println("ENTER HOW MANY CUSTOMER TO INSERT");
        int count = sc1.nextInt();
        Bank[] b = new Bank[count];

        for (int i = 0; i < count; i++) {
            System.out.println("ENTER CUSTOMER NAME");
            String name = sc1.next();
            System.out.println("ENTER INITIAL ACCOUNT BALANCE FOR OPENING ACCOUNT");
            double balance = sc1.nextDouble();
            b[i] = new Bank(name, balance);
        }
        boolean flag = true;

        while (flag) {
            System.out.println("SELECT MODE OF OPERATION");
            System.out.println("1:SEARCH ALL CUSTOMERS" +
                    "\n2:SEARCH BY ACCOUNT NO" +
                    "\n3:DEPOSIT AMOUNT" +
                    "\n4:WITHDRAW AMOUNT" +
                    "\n5:CHECK BALANCE" +
                    "\n6:EXIT");
            int choice = sc1.nextInt();
            int accNo;
            boolean found = false;
            switch (choice) {
                case 1:
                    System.out.println("ACC.NO\tNAME\tBALANCE");
                    System.out.println("=======================================");
                    for (Bank bank : b) {
                        bank.showAccountDetails();
                    }
                    break;
                case 2:
                    System.out.println("ENTER ACCOUNT NO TO SEARCH CUSTOMER DETAILS");
                    accNo = sc1.nextInt();
                    for (Bank bank : b) {
                        found = bank.searchAccount(accNo);
                        if (found) {
                            break;
                        }
                    }
                    if (!found) {
                        System.out.println("ACCOUNT NOT EXIST");
                    }
                    break;
                case 3:
                    System.out.println("ENTER ACCOUNT NO TO DEPOSIT AMOUNT");
                    accNo = sc1.nextInt();
                    for (Bank bank : b) {
                        found = bank.searchAccount(accNo);
                        if (found) {
                            System.out.println("ENTER AMOUNT TO BE DEPOSITED");
                            double amt = sc1.nextDouble();
                            bank.deposit(amt);
                            break;
                        }
                    }
                    if (!found) {
                        System.out.println("ACCOUNT NOT EXIST");
                    }
                    break;
                case 4:
                    System.out.println("ENTER ACCOUNT NO TO WITHDRAW AMOUNT");
                    accNo = sc1.nextInt();
                    for (Bank bank : b) {
                        found = bank.searchAccount(accNo);
                        if (found) {
                            System.out.println("ENTER AMOUNT TO BE WITHDRAW");
                            double amt = sc1.nextDouble();
                            bank.withdraw(amt);
                            break;
                        }
                    }
                    if (!found) {
                        System.out.println("ACCOUNT NOT EXIST");
                    }
                    break;
                case 5:
                    System.out.println("ENTER ACCOUNT NO TO CHECK ACCOUNT BALANCE");
                    accNo = sc1.nextInt();
                    for (Bank bank : b) {
                        found = bank.searchAccount(accNo);
                        if (found) {
                            bank.checkBalance();
                            break;
                        }
                    }
                    if (!found) {
                        System.out.println("ACCOUNT NOT EXIST");
                    }
                    break;
                case 6:
                    flag = false;
                    System.out.println("THANKS FOR VISIT...");
                    break;
                default:
                    System.out.println("ENTER VALID CHOICE");
                    break;
            }
        }
    }
}
