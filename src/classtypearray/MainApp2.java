package classtypearray;

import java.util.Scanner;

public class MainApp2 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("ENTER HOW MANY STUDENTS YOU WANT TO STORE");
        int count=sc1.nextInt();

        Student[] studentArray=new Student[count];

        for(int i=0;i<count;i++){
            System.out.println("ENTER STUDENT ID");
            int id=sc1.nextInt();
            System.out.println("ENTER STUDENT NAME");
            String name=sc1.next();
            System.out.println("ENTER STUDENT PERCENTAGE");
            double percentage= sc1.nextDouble();

            studentArray[i]=new Student(id,name,percentage);
        }

        System.out.println("****************************************");
        System.out.println("ID\tNAME\tPERCENTAGE");
        for (int i=0;i<count;i++){
            studentArray[i].displayData();
        }

    }
}
