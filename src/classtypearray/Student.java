package classtypearray;

public class Student {
    int studentId;
    String studentName;
    double studentPercentage;

    public Student(int studentId, String studentName, double studentPercentage) {
        this.studentId = studentId;
        this.studentName = studentName;
        this.studentPercentage = studentPercentage;
    }

    void displayData(){
        System.out.println(studentId+"\t"+studentName+"\t"+studentPercentage);
    }
}
