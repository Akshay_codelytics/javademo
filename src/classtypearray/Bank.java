package classtypearray;

import java.util.Random;

public class Bank {
    Random r1 = new Random();
    private final int accountNo = r1.nextInt(1267657898);
    private final String customerName;
    private double accountBalance;

    public Bank(String customerName, double accountBalance) {
        this.customerName = customerName;
        this.accountBalance = accountBalance;
        System.out.println("ACCOUNT OPENED SUCCESSFULLY");
    }

    void showAccountDetails() {
        System.out.println(accountNo + "\t" + customerName + "\t" + accountBalance);
    }

    void deposit(double amount) {
        accountBalance = accountBalance + amount;
        System.out.println(amount + " Rs CREDITED SUCCESSFULLY");
    }

    void withdraw(double amt) {
        if (amt <= accountBalance) {
            accountBalance = accountBalance - amt;
            System.out.println(amt + " Rs DEBITED FROM YOUR ACCOUNT");
        } else {
            System.out.println("INSUFFICIENT BALANCE");
        }
    }

    void checkBalance() {
        System.out.println("ACCOUNT BALANCE IS " + accountBalance);
    }

    boolean searchAccount(int accNo) {
        if (accountNo == accNo) {
            showAccountDetails();
            return true;
        } else {
            return false;
        }
    }

}
