package classtypearray;

public class StringDemo1 {

    public static void main(String[] args) {
        String s1="Java";
        String s2="J2ee";
        String s3="Java";

        System.out.println(s1.hashCode());
        System.out.println(s2.hashCode());
        System.out.println(s3.hashCode());

        StringDemo1 d1=new StringDemo1();
        System.out.println(d1.hashCode());
    }
}
