package demo;

import java.util.Scanner;

public class Central {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);

        System.out.println("ENTER SIZE OF AN ARRAY");
        int size=sc1.nextInt();

        int[] arr1=new int[size];

        System.out.println("ENTER ELEMENTS");

        for (int i = 0; i < size; i++) {
            arr1[i]= sc1.nextInt();
        }

        System.out.println("*************************");

        for (int i = 0; i < size; i++) {
            System.out.println(arr1[i]);
        }
    }
}
