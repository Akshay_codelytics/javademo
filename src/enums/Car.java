package enums;

public class Car {
    public void drive(TrafficLight light){
        switch (light){
            case RED:
                System.out.println("STOP");
                break;
            case GREEN:
                System.out.println("GO");
                break;
            case YELLOW:
                System.out.println("SLOW DOWN");
                break;
        }
    }
}
