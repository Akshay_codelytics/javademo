package javalibrary;

import java.util.Random;

public class OtpGeneration {
    public static void main(String[] args) {
        generateOtp(4);
    }

    private static void generateOtp(int i) {
        String otp="";
        Random r1=new Random();
        for (int a=1;a<=i;a++){
            otp=otp.concat(Integer.toString(r1.nextInt(5)));
        }
        System.out.println("Generated Otp "+otp);
    }
}
