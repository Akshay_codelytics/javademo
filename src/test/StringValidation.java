package test;

import java.util.Scanner;

public class StringValidation {
    public static void main(String[] args) {
        Registration r1=new Registration();
        Scanner sc1=new Scanner(System.in);
        System.out.println("ENTER NAME");
        String name=sc1.next();

        System.out.println("ENTER EMAIL");
        String email=sc1.next();

        System.out.println("ENTER PASSWORD");
        String password=sc1.next();

        System.out.println("CONFIRM PASSWORD");
        String confirm=sc1.next();

        r1.validation(name,email,password,confirm);
    }
}

class Registration{
    void validation(String name,String email,String password,String confirmPassword){
        if(email.contains("@")&&email.contains(".")){
            if(!(email.startsWith("@")||(email.startsWith(".")))){
                if(password.length()>=6){
                    if(password.equals(confirmPassword)){
                        System.out.println("Welcome "+name.toUpperCase());
                        System.out.println("EMAIL ID IS "+email.toLowerCase());
                    }
                    else {
                        System.err.println("PASSWORD AND CONFIRM PASSWORD SHOULD BE SAME");
                    }
                }
                else{
                    System.err.println("PASSWORD MUST CONTAIN AT LEAST 6 CHARACTERS");
                }
            }
            else{
                System.err.println("EMAIL SHOULD NOT START WITH @ OR .");
            }
        }
        else{
            System.err.println("INVALID EMAIL ID");
        }
    }
}
