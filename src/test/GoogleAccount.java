package test;

public class GoogleAccount {
    static int count;
    static GoogleAccount account;
    private GoogleAccount(){
        count++;
    }
    static GoogleAccount createAccount(){
        if(count==0) {
            account = new GoogleAccount();
        } else {
            System.out.println("Account already created");
        }
        return account;
    }
    void accessDrive(){
        System.out.println("Access Google Drive Features");
    }
}
