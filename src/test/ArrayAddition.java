package test;

import java.util.Scanner;

public class ArrayAddition {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        System.out.println("ENTER HOW MANY BILLS TO STORE");
        int size = sc1.nextInt();

        double[] bills = new double[size];

        System.out.println("ENTER BILL AMOUNT");
        for (int i = 0; i < bills.length; i++) {
            bills[i] = sc1.nextInt();
        }
        System.out.println("********************************************");
        System.out.println("FINAL BILL DETAILS ARE");
        double[] totalAmount = billCalculator(bills, size);
        double[] tips = tipCalculator(bills, size);

        System.out.println("BILL\tTIP\t\tTOTAL");
        System.out.println("---------------------------");
        for (int i = 0; i < totalAmount.length; i++) {
            System.out.println(bills[i] + "\t" + tips[i] + "\t" + totalAmount[i]);
        }
    }

    static double[] tipCalculator(double[] bills, int size) {
        double[] tipPerBill = new double[size];
        for (int i = 0; i < bills.length; i++) {
            if (bills[i] < 300) {
                tipPerBill[i] = bills[i] * 0.05;
            } else {
                tipPerBill[i] = bills[i] * 0.1;
            }
        }
        return tipPerBill;
    }

    static double[] billCalculator(double[] bills, int size) {
        double[] tips = tipCalculator(bills, size);

        double[] finalAmount = new double[size];
        for (int i = 0; i < bills.length; i++) {
            finalAmount[i] = bills[i] + tips[i];
        }
        return finalAmount;
    }
}
