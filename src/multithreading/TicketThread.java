package multithreading;

public class TicketThread extends Thread{
    private final TicketCounter t1;
    private final String passengerName;
    private final int numberOfSeats;

    public TicketThread(TicketCounter t1, String passengerName, int numberOfSeats) {
        this.t1 = t1;
        this.passengerName = passengerName;
        this.numberOfSeats = numberOfSeats;
    }

    @Override
    public void run() {
        t1.bookTicket(passengerName,numberOfSeats);
    }
}
