package multithreading;

public class TicketCounter {
    private int availableTickets=3;

    public synchronized void bookTicket(String passengerName,int numberOfSeats){
        if(availableTickets>=numberOfSeats && numberOfSeats>0){
            System.out.println("Hello "+passengerName+" : "+numberOfSeats+" Seats booked successfully");
            availableTickets-=numberOfSeats;
        }else {
            System.out.println("Hello "+passengerName+" Seats are not available");
        }
    }
}
