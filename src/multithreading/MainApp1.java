package multithreading;

public class MainApp1 {
    public static void main(String[] args) {
        TicketCounter t1=new TicketCounter();

        TicketThread p1=new TicketThread(t1,"Akshay",2);
        TicketThread p2=new TicketThread(t1,"Ajinkya",2);

        p1.start();
        p2.start();
    }
}
